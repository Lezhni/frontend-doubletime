$(document).ready(function() {
	$('.main-slider').owlCarousel({
        items: 1,
        itemsDesktop: false,
        itemsDesktopSmall: false,
        itemsTablet: false,
        itemsTabletSmall: false,
        itemsMobile: false,
        autoPlay: 4000,
        slideSpeed: 500,
        paginationSpeed: 500,
        navigation: false,
        pagination: true,
        theme: false,
        addClassActive: true,
    });

    $('.serv-desc-block h3').click(function() {
        if($(this).hasClass('opened')) {
            $(this).removeClass('opened').next('.textblock').slideUp();
        } else {
            $(this).addClass('opened').next('.textblock').slideDown();
        }
    });
    $('.single-gallery').each(function() {
        $(this).magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery: {
              enabled:true,
              navigateByImgClick: true,
              preload: [0,1]
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            }
        });
    }); 
});